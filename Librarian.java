package Console;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

	public class Librarian {
	public static void main(String args[]) throws ClassNotFoundException,SQLException{

		//Step 1 : Register the driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("Driver registered Successfully");
						
		//Step 2 : Establish the connection
			Connection con= DriverManager.getConnection("jdbc:mysql://localhost:3306/library_management", "Sunbeam", "Sunbeam");
			System.out.println("Connection established Successfully");
							
		//Step 3: Create Statement
			Statement st=con.createStatement();
							
		//Step 4: Execute
			int ch,id,ans, Experience,bookid;
			int bid,av,userid,price,isbn, copy;
			String email,phone,password,name, DOB, Address, classs, dept;
			String btitle, role, title, bauthor, dob, edit_pass,subject;			
								
do {
	System.out.println("0.Sign Out\n1.Sign In\n2.Edit Profile\n3.Edit Password\n4.Add New Books\n5.Edit Book\n6.Check Availability\n7.Find Book\n8.Issue Copy\n9.Return Copy\n10.Take Payment\n11.Add New Member\n12.Change Rack\n13.Add New Copy\nEnter Your Choice");
	Scanner sc= new Scanner(System.in);
	ch=Integer.parseInt(sc.nextLine());
								
ResultSet rs;
switch(ch) {

case 0:
	System.out.println("Sign Out");
	break;

case 1:
	//Sign In
	System.out.println("Enter Email ID., Enter Password");
	email=sc.nextLine();
	password=sc.nextLine();
	String s1="insert into sign_in(email,password) values('"+email+"','"+password+"')";
	st.execute(s1);
	System.out.println("Successfully Signed In");
	break;
									
case 2:
	//Edit Profile
	System.out.println("Enter Email, Enter Password, Edit Name, Edit Address");
	email=sc.nextLine();
	password=sc.nextLine();
	name=sc.nextLine();
	Address=sc.nextLine();
	String s2="insert into edit_profile(email, password, edited_name, edited_address) values('"+email+"','"+password+"','"+name+"','"+Address+"')";
	st.execute(s2);
	System.out.println("Profile Edited");
	break;

case 3:
	//Edit Password
	System.out.println("Enter email, Enter Password, Edit Password");
	email=sc.nextLine();
	password=sc.nextLine();
	edit_pass=sc.nextLine();
	String s3="insert into edit_password(Email, Password, Edit_Pass) values('"+email+"','"+password+"', '"+edit_pass+"')";
	st.execute(s3);
	System.out.println("Password Edited");
	break;	
					
case 4:
	//Add New Books	bookid | name | author | subject | price | isbn | copy
	System.out.println("Insert new book");
	System.out.println("Enter Book ID, Title of Book, Author of Book, Subject, Price, ISBN, Copy Available");
	bid=Integer.parseInt(sc.nextLine());
	btitle=sc.nextLine();
	bauthor=sc.nextLine();
	subject=sc.nextLine();
	price=Integer.parseInt(sc.nextLine());
	isbn=Integer.parseInt(sc.nextLine());
	copy=Integer.parseInt(sc.nextLine());
	String s4="insert into books(bookid, name, author, subject, price, isbn, copy) values("+bid+",'"+btitle+"','"+bauthor+"','"+subject+"', "+price+","+isbn+", "+copy+")";
	st.execute(s4);
	System.out.println("Book Insertion is Successful");
	break;

case 5:
	//Edit Books
	System.out.println("Enter Book ID");
	bid=sc.nextInt();
	String s5="select * from book_info where bid like '"+bid+"'";
	rs=st.executeQuery(s5);
	System.out.println(rs.getString(1)+"\t"+rs.getString(2)+"\t"+rs.getString(3)+"\t"+rs.getString(4)+"\t");
	break;
	
case 6:
	//Check Availability
	System.out.println("Enter Book ID");
	bid=sc.nextInt();
	String s6="select * from book_info where bid like '"+bid+"'";
	ResultSet rs1=st.executeQuery(s6);
	while(rs1.next()) {
	System.out.println(rs1.getString(1)+"\t"+rs1.getString(2)+"\t"+rs1.getString(3)+"\t"+rs1.getString(4)+"\t");
	}
	break;
	
case 7:
	//Find Book
	System.out.println("Enter Book Name (First 3 Characters)");
	title=sc.nextLine();
	String s7="select * from book_info where title like '"+title+"%'";
	ResultSet rs2=st.executeQuery(s7);
	while(rs2.next()) {
		System.out.println(rs2.getString(1)+"\t"+rs2.getString(2)+"\t"+rs2.getString(3)+"\t"+rs2.getString(4));
	}
	break;			
	
case 8:
//Book Issue
	String status;
	int flag=0;
	System.out.println("Enter Roll No. ");
	id=Integer.parseInt(sc.nextLine());
	System.out.println("Enter Book Id");
	bid=Integer.parseInt(sc.nextLine());
	String sq= "select status from student_book where bid='"+bid+"' and rno='"+id+"'";
	ResultSet rsl=st.executeQuery(sq);
	while(rsl.next()) {
		status =rsl.getString(1);
		if(status.equals("I")) {
		System.out.println("Already Issued");
		flag=1;
		break;
			}
		}
	if(flag==0) {
		String s8="select availability from book_info where bid='"+bid+"'";
		ResultSet rs3=st.executeQuery(s8);
		int avl;
	while(rs3.next()) {
		String av1=rs3.getString(1);
		avl=Integer.parseInt(av1);
		if(avl>0) {
		String s9="insert into student_book(rno,bid,doi,status) values('"+id+"','"+bid+"',NOW(),'I')";
		st.executeUpdate(s9);
		System.out.println("Issued");
		avl=avl-1;
		String s10="updated book_info set availability='"+avl+"', where bid='"+bid+"'";
		st.executeUpdate(s10);
		System.out.println("Updated");
		break;	
				}
			}
		}
case 9:
	//Book Return
		System.out.println("Enter Roll no.");
		id=Integer.parseInt(sc.nextLine());
		System.out.println("Enter Book Id :");
		bid=Integer.parseInt(sc.nextLine());
		flag=0;
		String sql="select status from student_book where bid="+bid+" and rno="+id+"";
		ResultSet rsl2=st.executeQuery(sql);
		while(rsl2.next()) {
			status=rsl2.getString(1);
		if(status.equals("R")) {
			System.out.println("Already Returned");
			flag=1;
			break;	
			}
		}
		if(flag==0) {
		String s9="select availability from book_info where bid="+bid+"";
		ResultSet rs4=st.executeQuery(s9);
		while(rs4.next()) {
			String av1=rs4.getString(1);
		int avl1=Integer.parseInt(av1);
			
			String s10="insert into student_book(rno,bid,doi,status) values("+id+","+bid+", NOW(),'R')";
			st.executeUpdate(s10);
			System.out.println("Returned");
			avl1=avl1+1;
			String s11="update book_info set availability='"+avl1+"' where bid="+bid+" ";
			st.executeUpdate(s11);
			System.out.println("Updated");
			break;
			}
		}
		break;

//case 10:
//		//Take Payment 
//		System.out.println("Enter Member id: ");
//		String midr= sc.next();
//		System.out.println("Enter Amount: ");
//		String amount= sc.next();
//		System.out.println("Payment recieved");
//		break;

case 11:
	//Add New Member	userid | name     | email              | phone      | password    | role
	System.out.println("Add New Member");
	System.out.println("Userid, Name, Email, Phone, Password, Role");
	userid=Integer.parseInt(sc.nextLine());
	name=sc.nextLine();
	email=sc.nextLine();
	phone=sc.nextLine();
	password=sc.nextLine();
	role=sc.nextLine();
	String s21="insert into users(userid,name,email,phone,password,role) values("+userid+",'"+name+"','"+email+"','"+phone+"','"+password+"','"+role+"')";
	st.execute(s21);
	System.out.println("Member Added");
	break;
		
//	case 12:
//					//Change Rack
//					
//					System.out.println("Enter book id: ");
//					String ebookid= sc.next();
//					System.out.println("Enter existing rack: ");
//					String eer= sc.next();
//					System.out.println("Enter New Rack: ");
//					String enewr= sc.next();
//					System.out.println("Rack changed successfully");
//					break;
case 13:
	//Add new copy
	//bookid | copy
	System.out.println("Enter book id: ");
	bid=Integer.parseInt(sc.nextLine());
	System.out.println("Enter number of copy: ");
	copy=Integer.parseInt(sc.nextLine());
	String s13="insert into books(bookid, copy) values("+bid+", "+copy+")";
	st.executeUpdate(s13);
	System.out.println("Copy Inserted");
	break;
			
		
default:
	System.out.println("Wrong Choice");
			}
			System.out.println("Do you want to continue(1/0)");
			ans=Integer.parseInt(sc.nextLine());
		}
		while(ans==1);
		System.out.println("*******Thank You*******");
		
			
			}}